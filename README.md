**DOCK JIB CRANE**

For the purpose of the subject Mechanical Design and Product Development (at AUTh), design and develop the lower base supports of a DOCK (JIB)CRANE. 
The following available MatLab scripts were written to facilitate an easier time with calculation, material selection and dimensioning of WT and W beams, and a Pintle Connection. Safety Factor should be above 3.  
