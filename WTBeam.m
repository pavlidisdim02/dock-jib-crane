clc
clear

%WTBeam
%Calculation of the bending shear stresses acting on a WT Beam
%Calculation of equivelant total stress 
%of a Dock (Jib) Crane base support connection, due to its weight.

W=100000*9.81/2
L=1.4
M=1/4*W*L

%Material Properties
%S355J0W
sigma_Y=345
tau_Y=145

%WTBeam Calculation
%Dimensions
d=401
b=386
tw=23.6
tf=41.9

E1=b*tf;
E2=(d-tf)*tw;
SE=E1+E2;
y1=(d-(tf/2));
y2=(d-tf)/2;
SyA=(E1*y1)+(E2*y2);
Y=SyA/SE;
I=((((1/12)*b*(tf^3))+(b*tf*((y1-Y)^2)))+(((1/12)*tw*((d-tf)^3))+(tw*(d-tf)*((y2-Y)^2))))*10^(-12);
cA=(d-Y)*10^(-3);

sigma_B=(M*cA/I)*10^(-6)

SF_B=sigma_Y/sigma_B

A=((b-tw)/2)*tf
tau=W/(2*A)
SF_S=tau_Y/tau

a=sigma_Y/tau_Y
J=(((b-tw)/2)^2)*tf/6
%J=(1/12)*((b-tw)/2)*tf*((((b-tw)/2)^2)+(tf^2));
sigma_iso=sqrt(((M/J)^2)+((a*W/(2*A))^2))
SF=sigma_Y/sigma_iso


%S355J0W

##d=490
##b=401
##tw=16.5
##tf=27.2

##d=401
##b=386
##tw=23.6
##tf=41.9

##d=399
##b=384
##tw=21.1
##tf=38.1