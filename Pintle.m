clc
clear

%Pintle
%Calculation of the shear stress acting on a pintle (double shear)
%of a Dock (Jib) Crane base support connection, due to its weight.

Mass=800000
Weight=0.7*Mass*9.81/8
L=1.4
Moment=1/4*Weight*L

%Material Properties
%S355J0W
sigma_Y=345
tau_Y=140

%Pintle Calculation
D=240
d=100
A=pi*(((D/2)^2)-((d/2)^2))
Q=0.8*800000*9.81/8

%Double Shear
tau=(2*Weight)/A
SF_t=tau_Y/tau
